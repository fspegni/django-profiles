import os

from setuptools import find_packages, setup


setup(
    name="django-profile",
    zip_safe=False,  # eggs are the devil.
    version="0.0.1",
    description="A fork of the popular django-profile by ... compatible with Django (compatible with Django 1.5)",
    long_description=open(os.path.join(os.path.dirname(__file__), "README.rst")).read(),
    author="Francesco Spegni",
    author_email="franceso.spegni@gmail.com",
    url="https://gitlab.com/fspegni/django-profile/",
    include_package_data=True,
    package_dir={"": "src"},
    packages=find_packages("src"),
    classifiers=[
        "Development Status :: 5 - Production/Stable",
        "Environment :: Web Environment",
        "Framework :: Django",
        "Framework :: Django :: 1.5",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: BSD License",
        "Operating System :: OS Independent",
        "Programming Language :: Python",
        "Topic :: Software Development :: Libraries :: Python Modules",
        "Programming Language :: Python",
        "Programming Language :: Python :: 2.7",
        "Topic :: Utilities",
    ],
    python_requires=">=2.7,<3.0",
    install_requires=["Django>=1.5,<1.6",],
)
